package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.db.DbResource;


public class TopRatedMoviesDao {
	public List<TopRatedMovies> findMovies() {
		List<TopRatedMovies> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDBConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				TopRatedMovies trm = new TopRatedMovies();
					trm.setTitle(rs.getString(1));
					trm.setYear(rs.getInt(2));
					trm.setGenre(rs.getString(3));
					trm.setRatings(rs.getInt(4));
					trm.setCategory(rs.getString(5));
					
					listOfMovie.add(trm);
			}
			} catch (Exception e) {
				System.out.println("The list of movies which are not found on database"+e);
			}
		return listOfMovie;


	}
}