package com.greatlearning.dao;

import java.sql.Connection.*;
import java.sql.ResultSet;
import java.util.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import com.greatlearning.bean.MoviesComing;
import com.greatlearning.db.DbResource;

public class MoviesComingDao{
	
	public List<MoviesComing> findAllProdcut() {
		List<MoviesComing> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDBConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				MoviesComing p = new MoviesComing();
					p.setId(rs.getInt(1));
					p.setTitle(rs.getString(2));
					p.setYear(rs.getInt(3));
					p.setGenre(rs.getString(4));
					p.setRatings(rs.getInt(5));
					p.setCategory(rs.getString(6));
					
					listOfMovie.add(p);
			}
			} catch (Exception e) {
				System.out.println("The list of movies which are not found on database"+e);
			}
		return listOfMovie;

	}
}
	

