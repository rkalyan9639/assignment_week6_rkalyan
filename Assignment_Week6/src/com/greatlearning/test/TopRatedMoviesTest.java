package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.dao.TopRatedMoviesDao;

public class TopRatedMoviesTest {
	TopRatedMoviesDao es = new TopRatedMoviesDao();
	List<TopRatedMovies> listOfmovies = es.findMovies();
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals(4, movie.getId());
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("Josh", movie.getTitle());
	
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals(2015, movie.getYear());
	
	}

	@Test
	public void testGetGenre() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("Family Movie", movie.getGenre());
	
	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("8", movie.getRatings());
	
	}

	@Test
	public void testGetCategory() {
		//fail("Not yet implemented");
		TopRatedMovies movie = listOfmovies.get(3);
		assertEquals("TopRatedMovies", movie.getRatings());
	
	}

}