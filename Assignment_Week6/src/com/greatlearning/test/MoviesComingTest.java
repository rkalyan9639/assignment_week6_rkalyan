package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;
import com.greatlearning.bean.MoviesComing;


public class MoviesComingTest {                    
	@Test
	public void testGetId() {
		System.out.println("getId");
		MoviesComing instance=new MoviesComing();
		int expresult=1;
		instance.setId(1);
		int result=instance.getId();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		System.out.println("setId");
		int setId=1;
		MoviesComing instance=new MoviesComing();
		instance.setId(1);
		assertEquals(instance.getId(), setId);
		//fail("Not yet implemented");
	}

	
	@Test
	public void testGetTitle() {
		System.out.println("getTitle");
		MoviesComing instance=new MoviesComing();
		String expresult="The Jobs";
		instance.setTitle("The Jobs");
		String result=instance.getTitle();
		assertEquals(expresult,result);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		System.out.println("setTitle");
		String setTitle="The Real Mafia";
		MoviesComing instance=new MoviesComing();
		instance.setTitle("The Real Mafia");
		assertEquals(instance.getTitle(), setTitle);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		System.out.println("getYear");
		MoviesComing instance=new MoviesComing();
		int expresult=2018;
		instance.setYear(2018);
		int result=instance.getYear();
		assertEquals(expresult,result);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		System.out.println("setYear");
		int setYear=2018;
		MoviesComing instance=new MoviesComing();
		instance.setYear(2018); 
		assertEquals(instance.getYear(), setYear);
		
		//fail("Not yet implemented");
	}

	@Test
	public void testGetGenre() {
		System.out.println("Real Story");
		MoviesComing instance=new MoviesComing();
		String expresult="Real Story";
		instance.setGenre("Real Story");
		String result=instance.getGenre();
		assertEquals(expresult,result);
		
		
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGenre() {
		System.out.println("Love Story");
		String setGenres="Love Story";
		MoviesComing instance=new MoviesComing();
		instance.setGenre("Love Story");
		assertEquals(instance.getGenre(),setGenres);
		
		//fail("Not yet implemented");
	}

}