package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.MoviesInTheatre;
import com.greatlearning.dao.MoviesInTheatreDao;

public class MoviesInTheatreTest {
	MoviesInTheatreDao movie = new MoviesInTheatreDao();
	
	List<MoviesInTheatre> listOfmovies = movie.findAllMovie();
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		MoviesInTheatre movie = listOfmovies.get(1);
		assertEquals(2, movie.getId());
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		MoviesInTheatre movie = listOfmovies.get(1);
		assertEquals("Mahanati", movie.getTitle());
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		MoviesInTheatre movie = listOfmovies.get(1);
		assertEquals(2019, movie.getYear());
		}

	@Test
	public void testGetGeners() {
		//fail("Not yet implemented");
		MoviesInTheatre movie = listOfmovies.get(1);
		assertEquals("Bank Robbery", movie.getGenre());
	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		MoviesInTheatre movie = listOfmovies.get(1);
		assertEquals("7", movie.getRatings());
	}
	
	@Test
	public void testGetCategory() {
		//fail("Not yet implemented");
		MoviesInTheatre movie = listOfmovies.get(1);
		assertEquals("MoviesInTheatre", movie.getCategory());
	}

}
